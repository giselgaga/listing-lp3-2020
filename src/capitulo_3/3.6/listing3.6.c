
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>

int main(){
    pid_t child_pid;
    
    // Crear un proceso hijo
    child_pid = fork();
    if(child_pid > 0)
        // Parent process
        sleep(15);
    else
        // Child process
        exit(0);
    
    return 0;
}
