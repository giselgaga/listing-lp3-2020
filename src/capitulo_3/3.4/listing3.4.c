
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>

int spawn(char* program, char** arg_list){
    pid_t child_pid;
    // Duplicar el proceso
    child_pid = fork();
    if(child_pid != 0)
        // Es el proceso padre
        return child_pid;
    else
    {
        // Ejecutar un nuevo programa
        execvp(program, arg_list);
        // execvp solo retorna si ocurre un error
        fprintf(stderr, "an error occurred in execvp\n");
        abort();
    }
}

int main(){
    // La lista de argumentos es el comando "ls"
    char* arg_list[] = {
        "ls", // argv[0], el nombre del programa
        "-l",
        "/",
        NULL  // la lista debe terminar con un NULL
    };
    
    // Crear el hijo del proceso en ejecucion "ls". Ignora el retorno
    
    spawn("ls", arg_list);
    printf("main program, done\n");
    
    return 0;
}
