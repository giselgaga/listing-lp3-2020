#include<signal.h>
#include<string.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<unistd.h>
#include<stdlib.h>

sig_atomic_t child_exit_status;

void clean_up_children_process(int signal_number){
    // Limpia el proceso hijo
    int status;
    wait(&status);
    // Guarda el codigo de salida en una variable global
    child_exit_status = status;
}

int main() {
    // Manejo de SIGCHLD llamando a clean_up_children_process
    struct sigaction sigchld_action;
    memset(&sigchld_action, 0, sizeof(sigchld_action));
    sigchld_action.sa_handler = &clean_up_children_process;
    sigaction(SIGCHLD, &sigchld_action, NULL);
    
    
    pid_t child_pid;
    
    child_pid = fork();
    if(child_pid > 0)
        // Parent process
        sleep(15);
    else
        // Child process
        exit(0);
    
    
    return 0;
}
