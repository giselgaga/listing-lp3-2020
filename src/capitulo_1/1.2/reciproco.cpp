#include <cassert>
#include "reciproco.hpp"

double reciproco(int i) {
    
    // i debe de ser no-neutro.
    assert (i != 0);
    return 1.0/i;
    
}
