#include<stdio.h>
#include<stdlib.h>

int main() {
    
    char* server_name = getenv("SERVER_NAME");
    
    if(server_name == NULL)
        // Si la variable de entorno SERVER_NAME no fue definida
        // usar el default
        server_name = "server.my.company.com";
    
    printf("accessing server %s\n", server_name);
    // Accediendo al servidor...
    
    return 0;
    
}