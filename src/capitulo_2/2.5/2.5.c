#include<stdlib.h>
#include<unistd.h>

// Un manejador de archivos temporales creado con write_temp_file
typedef int temp_file_handle;

// Escribe LENGTH bytes desde el BUFFER en un archivo temporal.
// Se corta el enlace con el temporal de manera inmediata.
// Retorna el manejador.

temp_file_handle write_temp_file(char* buffer, size_t lenght){
    
    // Crea el archivo y su nombre
    char temp_filename[] = "/tmp/temp_file.XXXXXX";
    int fd = mkstemp(temp_filename);
    // Cortar el enlace cuando se cierra el descriptor
    unlink(temp_filename);
    // Escribir el numero de bytes en el archivo
    write(fd, &lenght, sizeof(lenght));
    // Escribir el contenido
    write(fd, buffer, lenght);
    // Usar el descriptor del archivo como manejador
    return fd;
    
}

/* Lee el contenido del archivo temporal creado con write_temp_file.
 * El valor del retorno se asignar en el buffer de esos contenidos, donde
 * quien lo llama lo debe desasignar con free. */

char* read_temp_file(temp_file_handle temp_file, size_t* lenght){
    
    char* buffer;
    // El manejador del archivo temporal se convierte en el descriptor del archivo
    int fd = temp_file;
    // Volver al inicio del archivo
    lseek(fd, 0, SEEK_SET);
    // Leer el tamanho del contenido
    read(fd, lenght, sizeof (*lenght));
    // Asignar el buffer y leer el contenido
    buffer = (char*) malloc(*lenght);
    read(fd, buffer, *lenght);
    // Cerrar el descriptor
    close(fd);
    return buffer;
    
}

int main(){
    
    // Hacer
    return 0;
    
}