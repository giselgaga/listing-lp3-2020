#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

// Nombre del programa
const char* program_name;

/* Imprime la informacion de este programa al STREAM (normalmente stdout
   o stderr), sale del programa con EXIT_CODE. No retorna. */

void print_usage(FILE* stream, int exit_code) {
    
    fprintf(stream, "Usage: %s options [ inputfile ... ]\n", program_name);
    fprintf(stream,
            " -h --help            Display this usage information.\n"
            " -o --output filename Write output to file.\n"
            " -v --verbose         Print verbose messages.\n");
    exit(exit_code);
    
}

int main(int argc, char* argv[]) {
    
    int next_option;
    
    // Un string con opciones cortas validas
    const char* const short_options = "ho:v";
    // Un arreglo con opciones largas validas
    const struct option long_options[] = {
        { "help",       0, NULL, 'h'},
        { "output",     1, NULL, 'o'},
        { "verbose",    0, NULL, 'v'},
        { NULL,         0, NULL,  0 }
    };
    
    // El nombre del archivo a recibir, un output, o un NULL
    const char* output_filename = NULL;

    // Mostrar mensajes
    int verbose = 0;
    
    // Recordar el nombre del programa para agregarlo en mensajes
    program_name = argv[0];
    
    do {
        next_option = getopt_long(argc, argv, short_options, long_options, NULL);
        
        switch(next_option) {
            case 'h':   // -h or --help
                // El usuario pide informacion. Imprime la salida estandar
                // y sale con el codigo de salida 0 (terminacion normal)
                print_usage(stdout, 0);
                
            case 'o':   // -o or --output
                // Esta opcion toma un argumento, el nombre del archivo de salida
                output_filename = optarg;
                break;
                
            case 'v':   // -v or --verbose
                verbose = 1;
                break;
                
            case '?':   // Opcion no valida
                // Imprimir en la salida de errores y terminar con codigo 1 (terminacion no normal)
                print_usage(stderr, 1);
                
            case -1:    // Acabar
                break;
                
            default:    // Accion inesperada
                abort();
        }
        
    }while(next_option != -1);
    
    if(verbose) {
        int i;
        
        for(i = optind; i < argc; ++i)
            printf("Argument: %s\n", argv[i]);
        
    }
    return 0;
}