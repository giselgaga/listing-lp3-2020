//Listings 2.1

#include <stdio.h>

int main(int argc, char* argv[]) {

    printf("Nombre del programa: '%s'.\n", argv[0]);
    printf("Este programa tiene %d argumentos.\n", argc - 1);
    
    // Imprimimos los argumentos si los hay
    if(argc > 1) {
        int i;
        printf("Los argumentos son:\n");
        
        for(i = 1; i < argc; ++i)
            printf(" %s\n", argv[i]);
    }  
    return 0;
}